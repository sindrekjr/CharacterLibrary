using System;
using System.Reflection;

namespace CharacterLibrary.Alignment
{
    [Flags]
    public enum Alignment 
    { 
        [StrVal("Lawful Good")] LG,
        [StrVal("Neutral Good")] NG,
        [StrVal("Chaotic Good")] CG,
        [StrVal("Lawful Neutral")] LN,
        [StrVal("True Neutral")] TN,
        [StrVal("Neutral")] N = TN,
        [StrVal("Chaotic Neutral")] CN,
        [StrVal("Lawful Evil")] LE,
        [StrVal("Neutral Evil")] NE,
        [StrVal("Chaotic Evil")] CE 
    }

    public class StrVal : System.Attribute
    {
        private string _value;
        public string Value { get { return _value; } }
        public StrVal(string value) => _value = value;

        public static string GetStringValue(Enum value)
        {
            string output = null;
            Type type = value.GetType();

            FieldInfo fi = type.GetField(value.ToString());
            StrVal[] attrs =
            fi.GetCustomAttributes(typeof(StrVal), false) as StrVal[];
            
            if(attrs.Length > 0) output = attrs[0].Value;
            return output;
        }
    }
}