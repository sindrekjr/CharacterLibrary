using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Newtonsoft.Json;

namespace CharacterLibrary
{
    public abstract class Character
    {
        public ClassType ClassType { get; protected set; }

        public int Strength { get; protected set; } = 10;
        public int Agility { get; protected set; } = 10;
        public int Intelligence { get; protected set; } = 10;

        public string Name { get; set; }
        public int HP { get; set; }
        public int MaxHP { get; private set; }
        public int Stamina { get; private set; } = 10;
        public int THAR0 { get => 20 - ((Strength - 10) / 2); }
        public int ArmorRating { get => 10 - ((Agility - 10) / 2); }

        public Character(string name, int hp, int maxHp)
        {
            Name = name;
            HP = hp;
            MaxHP = maxHp;
        }

        public Character(string name, int hp) : this(name, hp, hp) { }

        public virtual void Move(int distance) => Console.WriteLine($"{Name} moves {distance}.");

        // Attack method which must be implemented at the class level where the conditions for attack are known
        // For example, a subclass of Character that uses a bow might have a chance to miss
        // Attack() should function as a wrapper for DoDamageTo() whenever it successfully attacks
        public virtual bool Attack(Character target)
        {
            Stamina--;
            if(AttackRoll() >= THAR0 - target.ArmorRating)
            {
                DoDamageTo(target, new Random().Next(Strength));
                return true;
            }
            else
            {
                return false;
            }
        }
        
        // Method which handles doing damage to another character
        protected void DoDamageTo(Character character, int damage) => character.TakeDamage(damage);
        
        // Method which handles taking damage for this character
        public void TakeDamage(int damage) => HP -= damage;

        public bool IsDead() => HP < 0;
        public bool IsHurt() => HP < MaxHP;

        protected int AttackRoll() => new Random().Next(1, 20);


        /********************/
        /********************/
        /* Static utilities */
        
        public static IEnumerable<Type> GetAllCharacterTypes() => Assembly.GetExecutingAssembly().GetTypes().Where(type => type.IsSubclassOf(typeof(Character)));


        /**
         * 
         *
         */
        private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All
        };

        public string ToJson() => JsonConvert.SerializeObject(this, JsonSettings);

        public static Character FromJson(string json) => JsonConvert.DeserializeObject<Character>(json, JsonSettings);

        /**
         *
         *******************/
    }

    [Flags]
    public enum ClassType { Base, Archetype }
}