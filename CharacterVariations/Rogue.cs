namespace CharacterLibrary
{
    public class Rogue : Character
    {
        public Rogue(string name, int agility = 16, int hp = 8) : base(name, hp) {
            Agility = agility;
        }
    }
}