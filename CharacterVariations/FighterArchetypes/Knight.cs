namespace CharacterLibrary
{
    public class Knight : Fighter
    {
        public Knight(string name) : base(name, hp: 12) { }
    }
}