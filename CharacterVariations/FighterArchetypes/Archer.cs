namespace CharacterLibrary
{
    public class Archer : Fighter
    {
        public Archer(string name) : base(name, strength: 10) {
            Agility = 16;
        }
    }
}