using System;

namespace CharacterLibrary
{
    public class Wizard : Character
    {
        public Wizard(string name, int intelligence = 16, int hp = 6) : base(name, hp) {
            Intelligence = intelligence;
        }

        public override bool Attack(Character target)
        {
            DoDamageTo(target, new Random().Next(Intelligence));
            return true;
        }
    }
}