namespace CharacterLibrary
{
    public class Fighter : Character
    {
        public Fighter(string name, int strength = 16, int hp = 10) : base(name, hp) {
            Strength = strength;
        }
    }
}